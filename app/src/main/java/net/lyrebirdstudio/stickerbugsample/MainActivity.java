package net.lyrebirdstudio.stickerbugsample;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.appbar.AppBarLayout;
import com.lyrebirdstudio.pattern.PatternHelper;
import com.lyrebirdstudio.sticker.StickerFrameLayout;
import com.lyrebirdstudio.sticker.StickerLibHelper;

public class MainActivity extends AppCompatActivity {

    Button expand;
    Button addSticker;
    View bottomView;
    AppBarLayout appBarLayout;
    StickerFrameLayout stickerFrameLayout;

    StickerLibHelper stickerLibHelper = new StickerLibHelper();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        expand = findViewById(R.id.buttonExpand);
        addSticker = findViewById(R.id.buttonAddSticker);
        bottomView = findViewById(R.id.bottomView);
        appBarLayout = findViewById(R.id.appbarlayout);

        stickerFrameLayout = findViewById(R.id.sticker_view_container);
        stickerLibHelper.hideForOncreate(this, stickerFrameLayout);

        expand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (bottomView.getVisibility() == View.VISIBLE) {
                    bottomView.setVisibility(View.GONE);
                    appBarLayout.setVisibility(View.VISIBLE);
                } else {
                    bottomView.setVisibility(View.VISIBLE);
                    appBarLayout.setVisibility(View.GONE);
                }
            }
        });

        addSticker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appBarLayout.setVisibility(View.GONE);
                stickerLibHelper.addStickerGalleryFragment(MainActivity.this, stickerFrameLayout, R.id.sticker_grid_fragment_container);
            }
        });

        stickerFrameLayout.setOnHierarchyChangeListener(new ViewGroup.OnHierarchyChangeListener() {
            @Override
            public void onChildViewAdded(View parent, View child) {
                appBarLayout.setVisibility(View.VISIBLE);
            }

            @Override
            public void onChildViewRemoved(View parent, View child) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        if (PatternHelper.onBackPressed(this) || stickerLibHelper.hideOnBackPressed()) {
            appBarLayout.setVisibility(View.VISIBLE);
            return;
        }

        super.onBackPressed();
    }
}
